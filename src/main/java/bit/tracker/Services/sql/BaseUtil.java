package bit.tracker.Services.sql;

import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;

public class BaseUtil {
  protected static Logger logger;
  static Vertx vertx;

  public BaseUtil() {
  }

  public static void init(Logger appLogger, Vertx appVertx) {
    logger = appLogger;
    vertx = appVertx;
  }

  public static void startTx(SQLConnection conn, Future<Void> done) {
    conn.setAutoCommit(false, done.completer());
  }

  public static void endTx(SQLConnection conn, Future<Void> done) {
    conn.commit(done.completer());
  }

  public static void execute(SQLConnection conn, String sql, Handler<Void> done) {
    conn.execute(sql, (res) -> {
      if (res.failed()) {
        throw new RuntimeException(res.cause());
      } else {
        done.handle(res.result());
      }
    });
  }

  public static void query(SQLConnection conn, String sql, Handler<ResultSet> done) {
    conn.query(sql, (res) -> {
      if (res.failed()) {
        throw new RuntimeException(res.cause());
      } else {
        done.handle(res.result());
      }
    });
  }

  public static boolean isNullOrEmpty(String input) {
    return null == input || input.isEmpty();
  }
}
