package bit.tracker.config;

import io.vertx.core.json.JsonObject;

public class ConfigurationManager {

    private static JsonObject config;

    public static JsonObject getConfig() {
        return config;
    }

    public static void setConfig(JsonObject config) {
        ConfigurationManager.config = config;
    }
}