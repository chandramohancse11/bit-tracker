package bit.tracker.exceptions;

/**
 * Created by chandra.rai on 20/06/18.
 */
public class UnAuthorizedException extends Exception {

    public UnAuthorizedException(String message) {
        super(message);
    }
}
