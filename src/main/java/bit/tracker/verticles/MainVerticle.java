package bit.tracker.verticles;

import bit.tracker.Services.HttpService;
import bit.tracker.handlers.BitStatisticsHandler;
import bit.tracker.utils.Utils;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.http.HttpServer;
import io.vertx.rxjava.ext.web.Router;
import io.vertx.rxjava.ext.web.handler.BodyHandler;
import java.util.Set;
import bit.tracker.config.ConfigurationManager;
import bit.tracker.handlers.HealthCheckHandler;
import bit.tracker.utils.LogFactory;
import io.vertx.rxjava.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;

public class MainVerticle extends AbstractVerticle {

  private static final Logger logger = LogFactory.getLogger(MainVerticle.class);
  public static Vertx vertxObj;

  @Override
  public void start(Future<Void> startFuture) throws Exception {
    vertxObj = vertx;
    ConfigurationManager.setConfig(config());
    undeployVerticles();

    deployVerticles();

    Router router = Router.router(vertx);

    prepareRoutes(router);

    HttpServer server = vertx.createHttpServer();

    server.requestHandler(router::accept).listen(config().getInteger("server_port", 8080));
    HttpService.initializeClient(
        new HttpClientOptions(config().getJsonObject("httpClientOptions", new JsonObject())));
    super.start(startFuture);

  }


  @Override
  public void stop(Future<Void> stopFuture) throws Exception {
    super.stop(stopFuture);
  }

  private void prepareRoutes(Router router) {
    router.post().handler(BodyHandler.create());
    router.patch().handler(BodyHandler.create());
    router.put().handler(BodyHandler.create());

    router.get("/health_check").handler(HealthCheckHandler::healthCheck);
    router.get("/api/v1/bitcoin/:statistics/forLast/:minutes").handler(BitStatisticsHandler::getBtcStatistics);

    router.route().failureHandler(ctx -> {
      Throwable exception = ctx.failure();
       if (exception instanceof DecodeException) {
        logger.error("Got decode Exception " + exception.getMessage());
        Utils.sendFailure(ctx.request().response(), "Invalid input",
            HttpResponseStatus.BAD_REQUEST.code());
      } else if (exception instanceof NumberFormatException) {
        logger.error("Got NumberFormatException " + exception.getMessage());
        Utils.sendFailure(ctx.request().response(), "Parameter should be numeric",
            HttpResponseStatus.BAD_REQUEST.code());
      } else if (exception instanceof Exception) {
        logger.error("Got Exception " + exception.getMessage());
        exception.printStackTrace();
        Utils.sendFailure(ctx.request().response(), "Something went wrong, Please try again later",
            HttpResponseStatus.INTERNAL_SERVER_ERROR.code());
      }
    });
  }

  private void deployVerticles() {
    DeploymentOptions deploymentOptions = new DeploymentOptions();
    deploymentOptions.setConfig(config());
    deploymentOptions.setInstances(1);

    vertx.deployVerticle("bit.tracker.verticles.SQLFetcher", deploymentOptions, res -> {
      if (res.succeeded()) {
        String deploymentID = res.result();
        logger.info("SQL Fetcher verticle deployed ok, deploymentID = " + deploymentID);
      } else {
        logger.error("SQL Fetcher verticle failed " + res.cause());
      }
    });

    vertx.deployVerticle("bit.tracker.verticles.BitCoinPriceDataIngester", deploymentOptions, res -> {
      if (res.succeeded()) {
        String deploymentID = res.result();
        logger.info("Bit Coin Price Data Ingester verticle deployed ok, deploymentID = " + deploymentID);
      } else {
        logger.error("Bit Coin Price Data Ingester verticle failed " + res.cause());
      }
    });

  }

  private void undeployVerticles() {
    Set<String> deploymemtIds = vertx.deploymentIDs();
    deploymemtIds.forEach(deploymentId -> {
      try {
        if (deploymentId != null && !deploymemtIds.isEmpty()) {
          vertx.undeploy(deploymentId, res -> {
            if (res.succeeded()) {
              logger.info("undeployment succesfull " + res.result());
            } else {
              logger.error("undeployment failed " + res.cause());
            }
          });
        }
      } catch (Exception e) {
        logger.error("Exception found while undeploying verticles");
      }
    });
  }

  public static WebClient getWebClient() {
    HttpClientOptions options = new HttpClientOptions();
    options.setLogActivity(true);
    options.setKeepAlive(true);
    options.setDefaultPort(443);
    options.setMaxPoolSize(ConfigurationManager.getConfig().getInteger("http_pool_size", 50));
    options.setSsl(true);
    WebClientOptions webClientOptions = new WebClientOptions(options);
    return WebClient.create(vertxObj, webClientOptions);
  }

}
