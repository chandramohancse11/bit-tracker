package bit.tracker.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class ArrayMapDeserializer extends JsonDeserializer<Map<String, Object>> {

  @Override
  public Map<String, Object> deserialize(JsonParser jp, DeserializationContext context)
      throws IOException {
    ObjectMapper mapper = (ObjectMapper) jp.getCodec();
    if (jp.getCurrentToken().equals(JsonToken.START_OBJECT)) {
      return mapper.readValue(jp, new TypeReference<LinkedHashMap<String, Object>>() {
      });
    } else {
      //consume this stream
      mapper.readTree(jp);
      return new LinkedHashMap<String, Object>();
    }
  }
}
