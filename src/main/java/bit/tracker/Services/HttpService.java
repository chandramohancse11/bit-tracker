package bit.tracker.Services;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.buffer.Buffer;
import io.vertx.rxjava.core.http.HttpClient;
import io.vertx.rxjava.core.http.HttpClientRequest;
import bit.tracker.Entities.APIDetails;
import bit.tracker.config.ConfigurationManager;
import bit.tracker.exceptions.UnAuthorizedException;
import bit.tracker.verticles.MainVerticle;
import rx.Observable;

import java.util.concurrent.TimeoutException;

/**
 * Created by chandra.rai
 */
public class HttpService {
    private static Logger logger = LoggerFactory.getLogger(HttpService.class);
    private static HttpClient httpClient;
    private static HttpClient httpsClient;

    public static HttpClient getClient(boolean ssl) {
        return ssl ? httpsClient : httpClient;
    }

    public static int getDefaultPort(boolean ssl) {
        return ssl ? 443 : 80;
    }

    public static boolean isSsl(APIDetails apiDetails) {
        return apiDetails.getProtocol().equals("https");
    }

    public static void initializeClient(HttpClientOptions options) {
        options.setLogActivity(true);
        options.setKeepAlive(true);
        options.setDefaultPort(443);
        options.setMaxPoolSize(ConfigurationManager.getConfig().getInteger("http_pool_size", 100));
        options.setPipelining(ConfigurationManager.getConfig().getBoolean("http_pipelining", false));
        options.setSsl(true);
        httpsClient = MainVerticle.vertxObj.createHttpClient(options);
        options.setDefaultPort(80);
        options.setSsl(false);
        httpClient = MainVerticle.vertxObj.createHttpClient(options);
    }

    private static String getpayload(APIDetails apiDetails) {
        if (apiDetails.getFormData() == null && apiDetails.getRawPayload() == null) {
            return "";
        }
        String contentType = apiDetails.getHeaders().get("content-type");
        if (contentType != null && contentType.equals("application/x-www-form-urlencoded")) {
            StringBuilder payload = new StringBuilder();
            apiDetails.getFormData().forEach((key, value) -> {
                payload.append(key).append("=").append(value).append("&");
            });
            return payload.toString();
        }
        return new JsonObject(apiDetails.getRawPayload()).toString();
    }

    public static Observable<Buffer> callAPI(APIDetails apiDetails) {
        boolean ssl = isSsl(apiDetails);
        int port = apiDetails.getPort() != null ? apiDetails.getPort() : getDefaultPort(ssl);
        return Observable.create(subscriber -> {
                    HttpClientRequest httpClientRequest = getClient(ssl)
                            .request(apiDetails.getMethod(), port, apiDetails.getHost(), apiDetails.getUrlWithURLParamters(),
                                    responseHandler -> {
                                        responseHandler.bodyHandler(body -> {
                                            subscriber.onNext(body);
                                            subscriber.onCompleted();
                                        });
                                        if (responseHandler.statusCode() != 200 && responseHandler.statusCode() != 204 && responseHandler.statusCode() != 201 && responseHandler.statusCode() != 409) {
                                            logger.error("Status code is : " + responseHandler.statusCode() + " for url "+ apiDetails.getCompleteURL());
                                            if(responseHandler.statusCode() == HttpResponseStatus.UNAUTHORIZED.code()){
                                                subscriber.onError(new UnAuthorizedException("Status code is : " + responseHandler.statusCode() + "\n"));
                                                subscriber.onCompleted();
                                            }else {
                                                subscriber.onError(new Exception("Status code is : " + responseHandler.statusCode() + "\n"));
                                                subscriber.onCompleted();
                                            }
                                        } else {
                                            logger.info("status code : " + responseHandler.statusCode());
                                        }
                                    }
                            );
                    if (apiDetails.getHeaders() != null) {
                        apiDetails.getHeaders().forEach(httpClientRequest::putHeader);
                    }
                    httpClientRequest.exceptionHandler(e -> {
                        logger.error(e.getMessage());
                        if(e instanceof TimeoutException){
                            logger.error("Timeout exception :" + apiDetails);
                        } else {
                            logger.error("Other type of exception: " + apiDetails);
                        }
                        logger.error("Received exception: " + e.getMessage());
                        subscriber.onError(e);
                        subscriber.onCompleted();
                    });
                    int timeout = apiDetails.getTimeout()!= null ? apiDetails.getTimeout() : ConfigurationManager.getConfig().getInteger("maxWait", 3000);
                    httpClientRequest.setTimeout(timeout);
                    httpClientRequest.end(getpayload(apiDetails));
                }
        );
    }
}
