package bit.tracker.Services.sql;


    import io.vertx.ext.jdbc.JDBCClient;

public class JDBCClientService {
  private static JDBCClient jdbcClient = null;
  private static JDBCClient jdbcClientSlave = null;

  public JDBCClientService() {
  }

  public static JDBCClient getJdbcClient() {
    return jdbcClient;
  }

  public static JDBCClient getJdbcClientSlave() {
    return jdbcClientSlave;
  }

  public static void setJdbcClient(JDBCClient jdbcClients) {
    jdbcClient = jdbcClients;
  }

  public static void setJdbcClientSlave(JDBCClient jdbcClient) {
    jdbcClientSlave = jdbcClient;
  }

  public static JDBCClient getJdbcClient(String readMode) {
    return readMode != null && readMode.equalsIgnoreCase("slave") ? jdbcClientSlave : jdbcClient;
  }
}
