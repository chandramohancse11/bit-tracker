package bit.tracker.handlers;

import bit.tracker.utils.LogFactory;
import io.vertx.core.logging.Logger;
import io.vertx.rxjava.ext.web.RoutingContext;

/**
 * Created by chandra.rai
 */
public final class HealthCheckHandler {

    private static final Logger logger = LogFactory.getLogger(HealthCheckHandler.class);

    public static void healthCheck(RoutingContext routingContext) {
        logger.info("I am 200 OK.");
        routingContext.response().end("I am 200 OK.\n");
    }
}
