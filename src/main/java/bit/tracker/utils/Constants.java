package bit.tracker.utils;

/**
 * Created by chandra.rai on 10/04/18.
 */
public final class Constants {

  private Constants() {

  }
  public static final int DUPLICATE_ENTITY_ERROR_CODE = 101;

  public final static String BTC_CURRENT_PRICE_INSERT = "INSERT INTO `btc_prices` "
      + "(`code`,`rate`,`at_time`)"
      + " VALUES "
      + "(?,?,?) ";

  public final static String GET_BTC_DATA = "select code, rate from btc_prices where at_time > ?  ";


}
