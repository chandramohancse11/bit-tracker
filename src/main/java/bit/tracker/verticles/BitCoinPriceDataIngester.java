package bit.tracker.verticles;

import bit.tracker.Entities.BTC;
import bit.tracker.Services.BtcStatsService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.rxjava.ext.web.client.WebClient;
import io.vertx.core.json.JsonObject;
import bit.tracker.config.ConfigurationManager;
import bit.tracker.utils.LogFactory;
import rx.Observable;

public class BitCoinPriceDataIngester extends AbstractVerticle {

  public static final Logger logger = LogFactory.getLogger(BitCoinPriceDataIngester.class);
  private static WebClient webClient = MainVerticle.getWebClient();
  private static String cryptoServiceEndPoint = ConfigurationManager.getConfig().getString("crypto_service_end_point");
  private static String cryptoServiceApi = ConfigurationManager.getConfig().getString("crypto_service_api");
  private static int apiTimeout = ConfigurationManager.getConfig().getInteger("apiTimeout");


  @Override
  public void start() throws Exception {
    Integer timer = ConfigurationManager.getConfig().getInteger("crypto_price_refresh_time");
    long timerID = vertx.setPeriodic(timer, new Handler<Long>() {

      @Override
      public void handle(Long aLong) {
        ingestBtcPrices();
      }
    });

  }

  public void ingestBtcPrices() {
      webClient.get(cryptoServiceEndPoint, cryptoServiceApi)
          .timeout(apiTimeout)
          .send(ar -> {
            if (ar.succeeded()) {
              JsonObject response = ar.result().bodyAsJsonObject();
              logger.info("BTC RESPONSE IS" + response);
              BTC btc= parseCoinDeskResponse(response);
              Observable<String> observable = BtcStatsService.postBtcCurrentPrice(btc);
              observable.subscribe(dbResponse->{
                logger.info("saved btc price at"+ System.currentTimeMillis() + " as ---"+ btc.getRate());
              },err -> {
                logger.error("btc price ingestion in db failed " , err.getCause());
                err.printStackTrace();
              });
            } else {
              logger.error("Something went wrong " + ar.cause().getMessage());
            }
          });
    }

  public BTC parseCoinDeskResponse(JsonObject response) {
    BTC btc = new BTC();
    btc.setCode(response.getJsonObject("bpi").getJsonObject("USD").getString("code"));
    btc.setRate(response.getJsonObject("bpi").getJsonObject("USD").getString("rate"));
    btc.setUpdated(response.getJsonObject("time").getString("updated"));
    btc.setUpdatedIso(response.getJsonObject("time").getString("updatedISO"));
    return btc;

  }



}







