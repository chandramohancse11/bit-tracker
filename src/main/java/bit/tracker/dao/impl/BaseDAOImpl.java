package bit.tracker.dao.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import bit.tracker.utils.Constants;
import bit.tracker.dao.BaseDAO;
import bit.tracker.exceptions.GenericError;
import bit.tracker.exceptions.GenericException;
import bit.tracker.utils.LogFactory;
import io.vertx.core.Future;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.eventbus.EventBus;
import java.util.List;
import rx.Observable;

/**
 * Created by chandra.rai on 20/08/18.
 */


public class BaseDAOImpl implements BaseDAO {

	private static final Logger logger = LogFactory.getLogger(BaseDAOImpl.class);
	protected static final EventBus eventBus = Vertx.currentContext().owner().eventBus(); //Vertx.currentContext().owner().eventBus();
	

	@Override
	public void create(String query, JsonArray params, Future<Integer> future) {
		JsonObject insertJson = new JsonObject();
		insertJson.put("statement", query);
		insertJson.put("type", "update");
		insertJson.put("params", params);
		logger.info("JSON send to SQLFetcher is : " + insertJson);
		logger.info("Event bus is =" + eventBus);
		eventBus.send("bit.tracker.sqlfetcher", insertJson, response -> {
			if (response.failed()) {
				ReplyException exception = (ReplyException) response.cause();
				logger.info("Failure code is : " + exception.failureCode());
				if (exception.failureCode() == Constants.DUPLICATE_ENTITY_ERROR_CODE) {
					future.fail(new GenericException(GenericError.DUPLICATE_ENTITY, "Entity already exists"));
				} else
					future.fail(response.cause());
			} else {
				String resBodyStr = (String) response.result().body();
				JsonObject responseJson = new JsonObject(resBodyStr);
				int entityId = responseJson.getJsonArray("keys").getInteger(0);
				future.complete(entityId);
			}

		});
	}

	@Override
	public <T> void findOne(String query, JsonArray params, Future<T> future, Class<T> clazz) {
		JsonObject searchJson = new JsonObject();
		searchJson.put("statement", query);
		searchJson.put("type", "query");
		searchJson.put("params", params);

		logger.info("JSON send to SQLFetcher is : " + searchJson);

		eventBus.send("bit.tracker.sqlfetcher", searchJson, response -> {
			if (response.failed()) {
				logger.info("Search by name failed :" + response.cause().getMessage());
				future.fail(response.cause());
			} else {
				JsonObject sqlResponseJson = new JsonObject(response.result().body().toString());
				int rows = sqlResponseJson.getJsonArray("rows").size();
				if (rows <= 0) {
					future.complete(null);
				} else {
					JsonObject json = sqlResponseJson.getJsonArray("rows").getJsonObject(0);
					logger.info("Response album JSON from SQl fetcher is :" + json);
					ObjectMapper objectMapper = new ObjectMapper();
					objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
					T t = null;
					try {
						t = objectMapper.readValue(json.toString(), clazz);
						future.complete(t);
					} catch (Exception e) {
						e.printStackTrace();
						future.fail(e);
					}
				}
			}
		});

	}

	@Override
	public <T> void find(Future<List<T>> findFuture, String searchQuery, JsonArray params, Class<T> clazz) {
		logger.info("Query to be executed is " + searchQuery);
		JsonObject searchQueryJson = new JsonObject();
		searchQueryJson.put("statement", searchQuery);
		searchQueryJson.put("type", "query");
		searchQueryJson.put("params", params);
		fireSelectQuery(findFuture, searchQueryJson, clazz);
	}

	@Override
	public <T> void findAll(Future<List<T>> findFuture, String searchQuery, JsonArray params, Class<T> clazz) {
		logger.info("Query to be executed is " + searchQuery);
		JsonObject searchQueryJson = new JsonObject();
		searchQueryJson.put("statement", searchQuery);
		searchQueryJson.put("type", "query");
		searchQueryJson.put("params", params);
		fireSelectQuery(findFuture, searchQueryJson, clazz);
	}

	@Override
	public void update(String query, JsonArray params, Future<Integer> future) {
		fireDeleteOrUpdateQuery(query, params, future);
	}

	@Override
	public void delete(String query, JsonArray params, Future<Integer> future) {
		fireDeleteOrUpdateQuery(query, params, future);
	}

	@Override
	public void count(String Query, Future<Long> future) {

	}

	public <T> void findByIds(Future<List<T>> findFuture, String searchQuery, Class<T> clazz) {
		logger.info("Query to be executed is " + searchQuery);
		JsonObject searchQueryJson = new JsonObject();
		searchQueryJson.put("statement", searchQuery);
		searchQueryJson.put("type", "query");
		eventBus.send("bit.tracker.sqlfetcher", searchQueryJson, response -> {
			if (response.failed()) {
				logger.error("Error in mysql query = " + searchQuery);
				logger.error("Cause = " + response.cause());
				findFuture.fail(response.cause());
			} else {
				JsonObject sqlResponseJson = new JsonObject(response.result().body().toString());
				JsonArray responseArray = sqlResponseJson.getJsonArray("rows");
				ObjectMapper mapper = new ObjectMapper();
				try {
					List<T> entities = mapper.readValue(responseArray.toString(),
							mapper.getTypeFactory().constructCollectionType(List.class, clazz));
					findFuture.complete(entities);
				} catch (Exception exception) {
					findFuture.fail(exception);
					logger.error(exception.getMessage());
				}
			}
		});
	}

	private <T> void fireSelectQuery(Future<List<T>> findFuture, JsonObject searchQueryJson, Class<T> clazz) {
		eventBus.send("bit.tracker.sqlfetcher", searchQueryJson, response -> {
			if (response.failed()) {
				logger.error("Error in  query execution for = " + searchQueryJson);
				logger.error("Cause = " + response.cause());
				findFuture.fail(response.cause());
			} else {
				JsonObject sqlResponseJson = new JsonObject(response.result().body().toString());
				logger.info("Success response from sqlfetcher is = " + sqlResponseJson);
				JsonArray responseArray = sqlResponseJson.getJsonArray("rows");
				ObjectMapper mapper = new ObjectMapper();
				mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
				try {
					List<T> entities = mapper.readValue(responseArray.toString(),
							mapper.getTypeFactory().constructCollectionType(List.class, clazz));
					findFuture.complete(entities);
				} catch (Exception exception) {
					findFuture.fail(exception);
					logger.error(exception.getMessage());
				}
			}
		});
	}

	private void fireDeleteOrUpdateQuery(String query, JsonArray params, Future<Integer> future) {
		JsonObject json = new JsonObject();
		json.put("statement", query);
		json.put("type", "update");
		json.put("params", params);
		logger.info("JSON send to SQLFetcher is : " + json);
		logger.info("Event bus is =" + eventBus);
		eventBus.send("bit.tracker.sqlfetcher", json, response -> {
			if (response.failed()) {
				ReplyException exception = (ReplyException) response.cause();
				if (exception.failureCode() == Constants.DUPLICATE_ENTITY_ERROR_CODE) {
					future.fail(new GenericException(GenericError.DUPLICATE_ENTITY, "Entity already exists"));
				} else
					future.fail(response.cause());
			} else {
				String resBodyStr = (String) response.result().body();
				JsonObject responseJson = new JsonObject(resBodyStr);
				logger.info("response from sqlfetcher is = " + responseJson);
				int rowsUpdated = responseJson.getInteger("updated");
				if(rowsUpdated <= 0) {
					future.fail(new GenericException(GenericError.ENTITY_NOT_FOUND, "Entity not found"));
				} else {
					future.complete(rowsUpdated);
				}
			}
		});
	}

	@Override
	public <T> void findOneV2(String query, JsonArray params, Future<T> future, Class<T> clazz) {
		JsonObject searchJson = new JsonObject();
		searchJson.put("statement", query);
		searchJson.put("type", "query");
		searchJson.put("params", params);

		logger.info("JSON send to SQLFetcher is : " + searchJson);

		eventBus.send("bit.tracker.sqlfetcher", searchJson, response -> {
			if (response.failed()) {
				logger.info("Search by name failed :" + response.cause().getMessage());
				future.fail(response.cause());
			} else {
				JsonObject sqlResponseJson = new JsonObject(response.result().body().toString());
				int rows = sqlResponseJson.getJsonArray("rows").size();
				if (rows <= 0) {
					future.fail(new GenericException(GenericError.ENTITY_NOT_FOUND, "Playlist not found"));
				} else {
					JsonObject json = sqlResponseJson.getJsonArray("rows").getJsonObject(0);
					logger.info("Response album JSON from SQl fetcher is :" + json);
					ObjectMapper objectMapper = new ObjectMapper();
					objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
					T t = null;
					try {
						t = objectMapper.readValue(json.toString(), clazz);
						future.complete(t);
					} catch (Exception e) {
						e.printStackTrace();
						future.fail(e);
					}
				}
			}
		});
	}
	
	@Override
	public <T> void findAll(String query, JsonArray params, Future<List<T>> findFuture, Class<T> clazz) {
		JsonObject searchQueryJson = new JsonObject();
		searchQueryJson.put("statement", query );
		searchQueryJson.put("type", "query");
		searchQueryJson.put("params", params);
		eventBus.send("bit.tracker.sqlfetcher", searchQueryJson, response -> {
			if (response.failed()) {
				logger.error("MySql statement failed = " +  query);
				logger.error("Cause = " + response.cause());
				findFuture.fail(response.cause());
			} else {
				JsonObject sqlResponseJson = new JsonObject(response.result().body().toString());
				logger.info("response = " + sqlResponseJson.toString());
				JsonArray responseArray = sqlResponseJson.getJsonArray("rows");
				ObjectMapper mapper = new ObjectMapper();
				mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
				try {
					List<T> entities = mapper.readValue(responseArray.toString(),
							mapper.getTypeFactory().constructCollectionType(List.class, clazz));
					findFuture.complete(entities);
				} catch (Exception exception) {
					findFuture.fail(exception);
					logger.error(exception.getMessage());
				}
			}
		});
	}

	@Override
	public <T> void findOne(String query, JsonArray params, Future<JsonObject> future) {
		JsonObject searchJson = new JsonObject();
		searchJson.put("statement", query);
		searchJson.put("type", "query");
		searchJson.put("params", params);

		logger.info("JSON send to SQLFetcher is : " + searchJson);

		eventBus.send("bit.tracker.sqlfetcher", searchJson, response -> {
			if (response.failed()) {
				logger.info("Search by name failed :" + response.cause().getMessage());
				future.fail(response.cause());
			} else {
				JsonObject sqlResponseJson = new JsonObject(response.result().body().toString());
				int rows = sqlResponseJson.getJsonArray("rows").size();
				if (rows <= 0) {
					future.complete(null);
				} else {
					JsonObject json = sqlResponseJson.getJsonArray("rows").getJsonObject(0);
					future.complete(json);
				}
			}
	});
		
	}
	
	@Override
	public void find(String query, JsonArray params, Future<JsonArray> future) {
		JsonObject searchQueryJson = new JsonObject();
		searchQueryJson.put("statement", query );
		searchQueryJson.put("type", "query");
		searchQueryJson.put("params", params);
		eventBus.send("bit.tracker.sqlfetcher", searchQueryJson, response -> {
			if (response.failed()) {
				logger.error("MySql statement failed = " +  query);
				logger.error("Cause = " + response.cause());
				future.fail(response.cause());
			} else {
				JsonObject sqlResponseJson = new JsonObject(response.result().body().toString());
				logger.info("response = " + sqlResponseJson.toString());
				JsonArray responseArray = sqlResponseJson.getJsonArray("rows");
				future.complete(responseArray);
			}
		});
	}

	@Override
	public void findAll(String query, Future<JsonArray> future) {
		JsonObject searchQueryJson = new JsonObject();
		searchQueryJson.put("statement", query );
		searchQueryJson.put("type", "query");
		eventBus.send("bit.tracker.sqlfetcher", searchQueryJson, response -> {
			if (response.failed()) {
				logger.error("MySql statement failed = " +  query);
				logger.error("Cause = " + response.cause());
				future.fail(response.cause());
			} else {
				JsonObject sqlResponseJson = new JsonObject(response.result().body().toString());
				logger.info("response = " + sqlResponseJson.toString());
				JsonArray responseArray = sqlResponseJson.getJsonArray("rows");
				future.complete(responseArray);
			}
		});
	}
	@Override
	public void findAll(String query, JsonArray params, Future<JsonArray> future) {
		JsonObject searchQueryJson = new JsonObject();
		searchQueryJson.put("statement", query );
		searchQueryJson.put("type", "query");
		searchQueryJson.put("params", params);
		eventBus.send("bit.tracker.sqlfetcher", searchQueryJson, response -> {
			if (response.failed()) {
				logger.error("MySql statement failed = " +  query);
				logger.error("Cause = " + response.cause());
				future.fail(response.cause());
			} else {
				JsonObject sqlResponseJson = new JsonObject(response.result().body().toString());
				logger.info("response = " + sqlResponseJson.toString());
				JsonArray responseArray = sqlResponseJson.getJsonArray("rows");
				future.complete(responseArray);
			}
		});
	}

	@Override
	public void findOne(String query, Future<JsonObject> future) {
		JsonObject searchQueryJson = new JsonObject();
		searchQueryJson.put("statement", query);
		searchQueryJson.put("type", "query");
		eventBus.send("bit.tracker.sqlfetcher", searchQueryJson, response -> {
			if (response.failed()) {
				future.fail(response.cause());
			} else {
				JsonObject sqlResponseJson = new JsonObject(response.result().body().toString());
				int rows = sqlResponseJson.getJsonArray("rows").size();
				if (rows <= 0) {
					future.fail(new GenericException(GenericError.ENTITY_NOT_FOUND, "Entity not found"));
				}
				JsonObject cxeDeviceJson = sqlResponseJson.getJsonArray("rows").getJsonObject(0);
				future.complete(cxeDeviceJson);
			}
		});
	}
	@Override
	public Observable<JsonArray> findAll(String query, JsonArray params, String dbReadMode) {
		return Observable.create(subscriber -> {
			logger.info("params = "+params.toString());
			logger.info("query = "+query);
			JsonObject searchQueryJson = new JsonObject();
			searchQueryJson.put("statement", query );
			searchQueryJson.put("type", "query");
			searchQueryJson.put("params", params);
			searchQueryJson.put("db_read_mode",dbReadMode);
			
			Vertx.currentContext().owner().eventBus().send("bit.tracker.sqlfetcher", searchQueryJson, response  -> {
				if (response.succeeded()) {
					JsonObject sqlResponseJson = new JsonObject(response.result().body().toString());
					JsonArray responseArray = sqlResponseJson.getJsonArray("rows");
		    	 	logger.info("response = "+ responseArray);
		    	 	subscriber.onNext(responseArray);
					subscriber.onCompleted();
				}else {
					logger.info("response failed= " + response.cause());
					subscriber.onError(response.cause());
					subscriber.onCompleted();
				}
	        });
		});
	}


	@Override
	public Observable<JsonArray> findAll(String query, JsonArray params) {
		return findAll(query,params, "");
	}
	@Override
	public Observable<String> update(String query, JsonArray params) {
		return Observable.create(subscriber -> {
			logger.info("params = "+params.toString());
			logger.info("query = "+query);
			JsonObject updateQuery = new JsonObject();
			updateQuery.put("statement", query );
			updateQuery.put("type", "update");
			updateQuery.put("params", params);
			
			Vertx.currentContext().owner().eventBus().send("bit.tracker.sqlfetcher", updateQuery, response  -> {
				if (response.succeeded()) {
					String resBodyStr = (String) response.result().body();
					JsonObject responseJson = new JsonObject(resBodyStr);
					logger.info("response from sqlfetcher is = " + responseJson);
					Integer rowsUpdated = responseJson.getInteger("updated");
					if(rowsUpdated <= 0) {
						subscriber.onError(new GenericException(GenericError.ENTITY_NOT_FOUND, "Entity not found"));
					} else {
						subscriber.onNext(rowsUpdated.toString());
					}
					subscriber.onCompleted();
				}else {
					logger.info("response failed= "+response.cause());
					ReplyException exception = (ReplyException) response.cause();
					if (exception.failureCode() == Constants.DUPLICATE_ENTITY_ERROR_CODE)
						subscriber.onError(new GenericException(GenericError.DUPLICATE_ENTITY, "Entity already exists"));
					else
						subscriber.onError(response.cause());
				}
	        });
		});
	}

	@Override
	public Observable<String> executeQueriesInTxn(JsonArray requests) {
		return Observable.create(subscriber -> {
			JsonObject json = new JsonObject();
			json.put("queries", requests);
			eventBus.send("bit.tracker.transactional", json, response -> {
				if (response.failed()) {
					subscriber.onError(new GenericException(GenericError.DB_TRANSACTION_FAILED, "TRANSACTION FAILED , MAYBE DUE TO DUPLICATE TRANSACTION"));
				} else {
					subscriber.onNext("successfully completed");
					subscriber.onCompleted();
				}
			});
		});
	}

	
}
