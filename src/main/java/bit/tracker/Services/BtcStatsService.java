package bit.tracker.Services;

import bit.tracker.Entities.BTC;
import bit.tracker.dao.BaseDAO;
import bit.tracker.dao.impl.BaseDAOImpl;
import bit.tracker.utils.Constants;
import bit.tracker.utils.LogFactory;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import rx.Observable;

/**
 * Created by chandra.rai on 22/08/18.
 */
public class BtcStatsService {

  private static Logger logger = LogFactory.getLogger(BtcStatsService.class);
  private static BaseDAO baseDAO = new BaseDAOImpl();


  public static Observable<String> postBtcCurrentPrice(BTC btc) {
    String query = Constants.BTC_CURRENT_PRICE_INSERT;
    Date date= null;
    String pattern = "MMM dd, yyyy HH:mm:ss z";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    try {
       date = simpleDateFormat.parse(btc.getUpdated());
    }catch(Exception e)
    {
      e.printStackTrace();
    }
    Instant instant =  date.toInstant() ;
    Timestamp expiresAt = Timestamp.from(instant);
    NumberFormat format = NumberFormat.getInstance(Locale.US);
    Number number = null;
    try {
     number = format.parse(btc.getRate());
    }catch(Exception e)
    {
      e.printStackTrace();
    }
    double d = number.doubleValue();
    JsonArray params = new JsonArray(new ArrayList<>(Arrays.asList(btc.getCode(), d, expiresAt.toString())));
    return baseDAO.update(query,params);
  }



  public static Observable<JsonObject> getBtcStatistics(String minutes , String stats) {
    int t = Integer.parseInt(minutes);
    int hours = t / 60; //since both are ints, you get an int
    int minute = t % 60;
    Date currentDate = new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(hours)- TimeUnit.MINUTES.toMillis(minute));
    java.sql.Timestamp requiredTimestamp = new java.sql.Timestamp(currentDate.getTime());
    String queryString = Constants.GET_BTC_DATA;
    JsonArray params = new JsonArray(
        new ArrayList<>(Arrays.asList(requiredTimestamp.toString())));
    return baseDAO.findAll(queryString, params).
        flatMap(
            dbResult -> {
              ArrayList<Double> ar= getRatesList(dbResult);
              double d=0;
              if(stats.equalsIgnoreCase("median"))
               d= findMedian( ar);
              if(stats.equalsIgnoreCase("mean")||stats.equalsIgnoreCase("average") )
                d= findMean( ar);
              JsonObject response = new JsonObject();
              response.put(stats,d);
              return Observable.just(response);
            }
        );
  }

  public static double findMean(ArrayList<Double> a)
  {
    int n = a.size();
    double sum = 0;
    for (int i = 0; i < n; i++)
      sum += a.get(i);

    return sum /n;
  }

  // Function for calculating median
  public static double findMedian(ArrayList<Double> a)
  {
    // First we sort the array
    int n = a.size();
    Collections.sort(a);

    // check for even case
    if (n % 2 != 0)
      return a.get(n/2);

    return (a.get((n - 1) / 2) + a.get(n / 2)) / 2.0;
  }


  public static ArrayList<Double> getRatesList(JsonArray jsonArray) {
    ArrayList<Double> ar = new ArrayList<>();
    for (int i = 0; i < jsonArray.size(); i++) {
      JsonObject btc = jsonArray.getJsonObject(i);
     Double d  = btc.getDouble("rate");
      ar.add(d);
    }
    return ar;
  }



}
