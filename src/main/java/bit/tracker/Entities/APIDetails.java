package bit.tracker.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.core.http.HttpMethod;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class APIDetails implements Serializable {
    @JsonProperty(required = true)
    private String protocol;
    @JsonProperty(required = true)
    private String relativeURI;
    @JsonProperty(required = true)
    private String host;
    @JsonProperty(required = true)
    private HttpMethod method;
    private Integer port;
    private Map<String, String> headers;
    private List<KeyVal<String, String>> urlParameters;
    private Map<String, Object> formData;
    private Map<String, Object> rawPayload;
    private Integer timeout;

    public void addHeader(String headerKey, String headerValue) {
        if (headers == null) {
            headers = new HashMap<>();
        }
        headers.put(headerKey, headerValue);
    }

    @JsonIgnore
    public String getHeader(String headerKey) {
        return headers.get(headerKey);
    }

    @JsonIgnore
    public String getCompleteURL() {
        return protocol + "://" + host + relativeURI;
    }

    @JsonIgnore
    public String getUrlWithURLParamters() {
        if (getUrlParameters() != null) {
            StringBuilder url = new StringBuilder();
            url.append(relativeURI).append("?");
            getUrlParameters().forEach((val) -> {
                url.append(val.getKey()).append("=").append(val.getValue()).append("&");
            });
            return url.toString();
        } else
            return relativeURI;
    }

    @JsonIgnore
    public List<KeyVal<String, String>> getParameters() {
        if (method.equals(HttpMethod.GET)) {
            return null;//recursive call?
        } else {
            if (formData != null) {
                return formData.keySet().stream().map((key) -> (new KeyVal<>(key, formData.get(key).toString())))
                        .collect(Collectors.toList());
            }
            return null;
        }
    }

    public void addURLParameter(String key, String value) {
        if (urlParameters == null) {
            urlParameters = new ArrayList<>();
        }
        urlParameters.add(new KeyVal<>(key, value));
    }

    public void addRawPayloadKey(String key, String value){
        if(rawPayload == null){
            rawPayload = new LinkedHashMap<>();
        }
        rawPayload.put(key, value);
    }

    public void addFormData(String key, String value){
        if(formData == null){
            formData = new LinkedHashMap<>();
        }
        formData.put(key, value);
    }
}
