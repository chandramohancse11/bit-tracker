This project gives statistical data of Bit coin prices over last X minutes.

It is written in Vert.x which is highly scalable single threaded with event loop functionalities java framework.

My sql db is used for storing bit coin prices. We could have used prometheus which is time-series database but to pull metrics out of it we need tools like graffana or some python code. Prometheus doesn't have any java client to pull metrics , so i dropped the idea because we need java rest api's for this project.

Steps to run the program :
1.Mysql server should be up and running.
2.There is sql directory under project which has sqlDetails file which contains database and table creation query.
3.open terminal , go under project directory i.e cd bit-tracker.
4.From this location to compile and build jar run command : mvn clean install.
5.Now to run the application run command from same location : java -jar target/bit-tracker-0.0.1-SNAPSHOT-fat.jar -conf config/conf.json.

After Application is up we can hit below curls to get median , mean or average of btc prices over last X minutes. Also after application is up wait for some time before hitting API's so that our db gets flooded with BTC price data from coin desk server. Note: There is file named as conf.json under config directory which contains all configurations. Currently running cron at every 1 minutes to hit coindesk api to get current price of bitcoin and insert same in our mysql DB. Also interval for getting data from coin desk is configurable by editing key "crypto_price_refresh_time" in conf.json file.

Curls:

GET MEDIAN FOR PAST X MINUTES

curl -X GET \ http://localhost:8080/api/v1/bitcoin/median/forLast/20 \ -H 'api-key: ddbe3c7c-14c1-4b83-ad1d-978913e32cdd' \ -H 'cache-control: no-cache' \ -H 'postman-token: b7347a2d-7634-ee5a-c2a6-ca7369df07a6'

GET MEAN FOR PAST X MINUTES 

curl -X GET \ http://localhost:8080/api/v1/bitcoin/mean/forLast/20 \ -H 'api-key: ddbe3c7c-14c1-4b83-ad1d-978913e32cdd' \ -H 'cache-control: no-cache' \ -H 'postman-token: 859a530f-63db-0ffb-53e0-d6864bfc86be'