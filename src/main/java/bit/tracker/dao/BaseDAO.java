package bit.tracker.dao;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.List;
import rx.Observable;

public interface BaseDAO {

//	Below methods prevent SQL Injection 
	public  void create(String query, JsonArray params, Future<Integer> future);
	public <T> void findOne(String query, JsonArray params, Future<T> future, Class<T> clazz) ;
	public <T> void find(Future<List<T>> findFuture, String searchQuery, JsonArray params,
      Class<T> clazz);
	public <T> void findAll(Future<List<T>> findFuture, String searchQuery, JsonArray params,
      Class<T> clazz);
	public void update(String query, JsonArray params, Future<Integer> future);
	public void delete(String query, JsonArray params, Future<Integer> future);
	public void count(String Query, Future<Long> future);
	public <T> void findOneV2(String query, JsonArray params, Future<T> future, Class<T> clazz) ;
	public <T> void findByIds(Future<List<T>> findFuture, String searchQuery, Class<T> clazz);
	public <T> void findAll(String query, JsonArray params, Future<List<T>> future, Class<T> clazz) ;
	public <T> void findOne(String query, JsonArray params, Future<JsonObject> future) ;
	public  void find(String query, JsonArray params, Future<JsonArray> future) ;
	public  void findAll(String query, Future<JsonArray> future) ;
	public void findOne(String query, Future<JsonObject> future);
	void findAll(String query, JsonArray params, Future<JsonArray> future);
	Observable<JsonArray> findAll(String query, JsonArray params);
	Observable<String> update(String query, JsonArray params);
	Observable<JsonArray> findAll(String query, JsonArray params, String dbReadMode);
	public Observable<String> executeQueriesInTxn(JsonArray requests);

}