package bit.tracker.handlers;

import bit.tracker.Services.BtcStatsService;
import bit.tracker.utils.LogFactory;
import bit.tracker.utils.Utils;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.rxjava.core.http.HttpServerRequest;
import io.vertx.rxjava.ext.web.RoutingContext;
import java.util.UUID;
import rx.Observable;

/**
 * Created by chandra.rai
 */

public class BitStatisticsHandler {

  private static Logger logger = LogFactory.getLogger(BitStatisticsHandler.class);

  public static void getBtcStatistics(RoutingContext routingContext) {
    Utils.validateAPIKey(routingContext);
    HttpServerRequest request = routingContext.request();
    String requestId = UUID.randomUUID().toString();
    logger.info("GET Bit Coin Statistics call with requestId request: " + requestId
        + ": Headers" + Utils.getJson(routingContext.request().headers().getDelegate().entries())
        + ". \t Query params: "+Utils.getJson(routingContext.request().params().getDelegate().entries())
        + ". \t Body: "+Utils.getJson(routingContext.getBodyAsJson()));
    JsonObject apiResponse = new JsonObject();
    Observable<JsonObject> observable = BtcStatsService.getBtcStatistics(routingContext.request().params().get("minutes"),routingContext.request().params().get("statistics"));
    observable.subscribe(response->{
      JsonObject transformedJson = response;
      apiResponse.put("data", transformedJson);
      Utils.sendSuccess(apiResponse,request.response(),201);
    },err -> {
      logger.error("GET Bit Coin Statistics call with requestId request: " + requestId, err);
      err.printStackTrace();
      Utils.sendFailure(request.response(), err.getMessage(), HttpResponseStatus.BAD_REQUEST.code());
    });
  }


}
